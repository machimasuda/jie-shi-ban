package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	String fromDate = request.getParameter("fromDate");
        request.setAttribute("fromDate", fromDate);
    		if (fromDate == null || fromDate.isEmpty() || fromDate.length() == 0){
    			fromDate = "2018-01-01 00:00:00";
    		}


    	String toDate = request.getParameter("toDate");
        request.setAttribute("toDate", toDate);
    		if(toDate == null || toDate.isEmpty() || toDate.length() ==0 ){
    			Date date = new Date();
    			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    			toDate = sdf1.format(date);
    		}
    		toDate = toDate + " 23:59:59";

    		String Category = request.getParameter("Category");



        List<UserMessage> messages = new MessageService().getMessage(fromDate,toDate,Category);
        request.setAttribute("messages", messages);

        if(messages==null){

        }


        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);
        request.setAttribute("Category", Category);
    	request.getRequestDispatcher("/top.jsp").forward(request, response);

    }
}

