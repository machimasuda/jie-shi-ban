package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;


@WebServlet(urlPatterns = { "/commentdelete" })
public class CommentDeletedServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
         HttpServletResponse response) throws ServletException, IOException {

    	int comment_id = Integer.parseInt(request.getParameter("comment_id"));
    	new CommentService().deleteComment(comment_id);

    	response.sendRedirect("./");
    }
}