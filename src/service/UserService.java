package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import userDao.UserDao;
import util.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if (!StringUtils.isEmpty(user.getPassword()) == true){
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

private static final int LIMIT_NUM = 1000;

	public List<User> getUser() {

    Connection connection = null;
    try {
        connection = getConnection();

        UserDao userDao = new UserDao();
        List<User> ret = userDao.getAllUser(connection, LIMIT_NUM);

        commit(connection);

        return ret;
    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
	}

	public User getUser(int userId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getEditUser(connection, userId);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	public User getUser(String account) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getExistUser(connection, account);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if (StringUtils.isEmpty(user.getPassword()) == false){
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



	public void isdeleted(int id, int is_deleted) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.UpdateIsdeleted(connection, id,is_deleted);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}