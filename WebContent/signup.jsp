<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<!-- <br/><div class="alltitle">ユーザー新規登録</div>
	<a href="management" class="home">ユーザー管理画面に戻る</a> -->
	</head>
	<body>
		<div class ="head">
			<a href="management" class="home">ユーザー管理</a>
			<a href="logout" class="loginiNewUser" onclick ="return loginout();">ログアウト</a>
			<div class="alltitle">ユーザー新規登録</div>
		</div>
		<div class="main-contents">

            <div class="shinnki">
            <c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
                    </ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
            </c:if>
			<form action="signup" method="post">
			<table align="center"cellspacing="9" style="text-align:left;">
				<tr>
				<td><label for="name">名前</label></td>
				<td style="width:220px;"><input name="name" value="${User.name}" placeholder="10文字以下" id="name"/><br/>
				</td>
				</tr>
				<tr>
				<td><label for="account">ログインID</label>
				<input type="hidden" name="beforesignAccount" value="${User.account}" /></td>
				<td style="width:220px;"><input name="account"value="${User.account}" placeholder="半角英数字の6-20文字" id="account"/><br/>
				</td>
				</tr>

				<tr>
				<td><label for="password">パスワード</label></td>
				<td style="width:220px;"><input name="password"  type="password" placeholder="半角英数字記号の6-20文字" ><br/>
				</td>
				</tr>

				<tr><td><label for="checkpassword">確認用パスワード</label></td>
				<td style="width:220px;"><input name="checkpassword"  type="password"></td>
				</tr>

					<tr>
					<td><label for="branch_id">支店名</label></td>
						<td><select name="branch_id" class="senntaku">
							<option value="">選択してください</option>
							<c:forEach items="${branches}" var="branch">
							<c:if test="${User.branch_id == branch.id}" >
							<option value="${branch.id}" selected>${branch.name}</option>
							</c:if>
							<c:if test="${User.branch_id != branch.id}" >
							<option value="${branch.id}">${branch.name}</option>
							</c:if>
							</c:forEach>
						</select>
						</td>
					</tr>

					<tr>
					<td><label for="department_id">部署・役職名</label></td>
						<td><select name="department_id" class="senntaku">
							<option value="" >選択してください</option>
					<c:forEach items="${departments}" var="department">
							<c:if test="${User.department_id == department.id}">
							<option value="${department.id}"selected>${department.name}</option>
							</c:if>
							<c:if test="${User.department_id != department.id}">
							<option value="${department.id}">${department.name}</option>
							</c:if>
						</c:forEach>
						</select>
						</td></tr>
</table>
						<br/><input class="botann" type="submit" value="登録" />
            </form>
			</div>
			</div>
    </body>
    <div id="footer">Copyright(c)Masuda Machi</div>
</html>