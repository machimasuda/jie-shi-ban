<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
		<body>
			<div class ="head">
				<div class="users">
					<a href="./" class="home">ホーム</a>  <a href="signup" class="yu_za">ユーザー新規登録</a>
					<a href="logout" class="loginManagement" onclick ="return loginout();">ログアウト</a>
					<div class="alltitle">ユーザー管理</div>
				</div>
			</div>


			<div class="main-contents">

            	<div id="userManagementTable">
            		<c:if test="${not empty errorMessages }">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${ errorMessages }" var="message">
									<li><c:out value="${message}" />
								</c:forEach>
                    		</ul>
						</div>
						<c:remove var="errorMessages" scope="session" />
            		</c:if>
					<table class="userList" border="1" >

						<tr bgcolor="#e6f58b">
							<th>名前</th>
							<th>ログインID</th>
							<th>支店名</th>
							<th>部署・役職名</th>
							<th>状態</th>
							<th>停止・復活</th>
							<th>編集</th>
						</tr>

						<c:forEach items="${users}" var="user">
						<tr>
    						<td><span class="name"><c:out value="${user.name}" /></span></td>
           					<td><div class="account"><c:out value="${user.account}" /></div></td>
            				<td><div class="branch_id "><c:out value="${user.branch_name }" /></div></td>
            				<td><div class="department_id "><c:out value="${user.department_name }" /></div></td>

							<td align="center">
	            				<div class="is_deleted">
	            					<c:if test="${loginUser.id == user.id}">
	            					<div class="StateLogin"><c:out  value="ログイン中"/></div>
	            					</c:if>
	                				<c:if test="${loginUser.id != user.id}">
										<form action ="IsdeletedServlet" method ="post">
		 									<c:if test="${user.is_deleted == 0}">
		                						<div class="State"><c:out  value="活動中"/></div>
		               						</c:if>
											<c:if test="${user.is_deleted == 1}">
												<div class="StateStop"><c:out  value="停止中" /></div>
											</c:if>
										</form>
									</c:if>
								</div>
							</td>


							<td align="center">
	            				<div class="is_deleted">
	                				<c:if test="${loginUser.id != user.id}">
										<form action ="IsdeletedServlet" method ="post">
		 									<c:if test="${user.is_deleted == 0}">
		 										<input type ="hidden" name ="user_id" value ="${user.id}"/>
		                						<input type ="hidden" name ="is_deleted" value ="1" />
		                						<input class="teishi" type="submit" value="停止" onclick ="return check();"/>
		               						</c:if>
											<c:if test="${user.is_deleted == 1}">
												<input type ="hidden" name ="user_id" value ="${user.id}"/>
												<input type ="hidden" name ="is_deleted" value ="0" />
												<input class="hukkatu" type="submit"  value="復活" onclick ="return Movecheck();">
											</c:if>
										</form>
									</c:if>
								</div>
							</td>
							<td align="center"><a href="settings?id=${user.id}" >編集</a></td>
						</tr>
   						</c:forEach>

					</table>
				</div>
			</div>

			<script>
				function check() {
    				return confirm('このアカウントを停止してよろしいですか？');
				}
				function Movecheck() {
	    			return confirm('このアカウントを復活してよろしいですか？');
				}
			</script>
		</body>
		<div id="footer">Copyright(c)Masuda Machi</div>
</html>