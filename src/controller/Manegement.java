package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class Manegement extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> user = new UserService().getUser();

        request.setAttribute("users", user);



    	request.getRequestDispatcher("/UserManegement.jsp").forward(request, response);

    }


    @Override
    protected void doPost(HttpServletRequest request,
    	            HttpServletResponse response) throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");

        List<User> users = new UserService().getUser();
        request.setAttribute("users", users);
    }
}
