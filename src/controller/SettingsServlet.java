package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;
@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
	private static final String checkpassword = null;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {


    	HttpSession session = request.getSession();

    	List<Branch> branchList = new BranchService().getBranch();
    	session.setAttribute("branches", branchList);

    	List<Department> departmentList = new DepartmentService().getDepartment();
    	session.setAttribute("departments", departmentList);

        User User = (User) session.getAttribute("EditUser");

        String strId = request.getParameter("id");
        List<String> messages = new ArrayList<String>();
        if(!strId.matches("[0-9]+$")){
        	messages.add("不正なパラメーターが発生しました");
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("management");
        	return;
        }
        int user_id = Integer.parseInt(strId);



        String user_account = request.getParameter("account");
        session.setAttribute( "user_account",user_account);



        User editUser = new UserService().getUser(user_id);
        if(editUser==null){
        	messages.add("不正なパラメーターが発生しました");
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("management");
        	return;
        }
        request.setAttribute("editName",editUser.getName());
        request.setAttribute("editUser", editUser);

        User existUser = new UserService().getUser(user_account);
        request.setAttribute("existUser", existUser);


        request.getRequestDispatcher("settings.jsp").forward(request, response);


    }

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();

    	User editUser = getEditUser(request);

    	if (isValid(request, messages) == true) {
    		new UserService().update(editUser);
    		response.sendRedirect("management");
    	} else {
    		request.setAttribute("errorMessages", messages);
    		request.setAttribute("editUser", editUser);
    		request.setAttribute("editName",request.getParameter("editName"));
    		request.getRequestDispatcher("settings.jsp").forward(request, response);
    	}
    }

	private User getEditUser(HttpServletRequest request)
        throws IOException, ServletException {

		User editUser = new User();{
			editUser.setId(Integer.parseInt(request.getParameter("id")));
			editUser.setName(request.getParameter("name"));
			editUser.setAccount(request.getParameter("account"));
			editUser.setPassword(request.getParameter("password"));
			editUser.setBranch_id(request.getParameter("branch_id"));
			editUser.setDepartment_id(request.getParameter("department_id"));
			return editUser;
		}
	}



	private boolean isValid(HttpServletRequest request, List<String> messages) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String checkpassword = request.getParameter("checkpassword");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int department_id = Integer.parseInt(request.getParameter("department_id"));


		User editUser = new UserService().getUser(account);
        	request.setAttribute("editUser", editUser);

		if (StringUtils.isBlank(name) == true) {
	        messages.add("名前を入力してください");
	    }else if (name.length() >= 10)  {
	        messages.add("名前は10文字以下で入力してください");
	    }
        if (StringUtils.isBlank(account) == true) {
            messages.add("ログインIDを入力してください");
        }else if (account.length() > 20 || account.length() < 6)   {
            messages.add("ログインIDは6文字以上20文字以下で入力してください");
        }
        if (!account.matches("^[0-9a-zA-Z]*$"))  {
            messages.add("ログインIDは半角英数字で入力してください");
        }

        if(editUser != null && editUser.getId() != id ){
        	messages.add("このログインIDは既に使われています");
        }

        if (StringUtils.isBlank(password) == false) {
        	if(password.length() > 20 || password.length() < 6){
           	 messages.add("パスワードは6文字以上20文字以下で入力してください");
        	}else{
        		if (!password.matches("^[a-zA-Z0-9 \\-\\/\\:\\-\\@\\[\\-\\{\\-\\~]*$")) {
                messages.add("パスワードは半角英数字記号で入力してください");
        		}else if(password.matches("^.*\\s.*$$S")){
            	messages.add("パスワードは半角英数字記号で入力してください");
        		}else if(StringUtils.isBlank(checkpassword) == true){
            	messages.add("確認用パスワードを入力してください");
        		}else {
        			if(!password.equals(checkpassword)){
        				messages.add("パスワードが一致しません");
        			}
        		}
        	}
        }else if(!StringUtils.isEmpty(password)){
        	messages.add("パスワードは半角英数字記号で入力してください");

        }

        if(branch_id==1){
        	if(!(department_id==1 ||department_id==2)){
        		messages.add("支店名と部署・役職名の組み合わせが不正です");
        	}
        }

        if(branch_id !=1){
        		if(department_id==1 ||department_id==2){
        		messages.add("支店名と部署・役職名の組み合わせが不正です");
        	}
        }

        if (messages.size() == 0) {
        	return true;
		} else {
			return false;
		}
	}
}