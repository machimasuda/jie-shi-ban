package userDao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append(" text");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(", message_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?");//user_id
            sql.append(", ?");//message_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getUser_Id());
            ps.setInt(3, comment.getMessage_id());

            System.out.println(ps.toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }



	public List<Comment> getComment(Connection connection, int limitNum) {
		//
		return null;
	}

	public void deleteComment(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "delete from comments where id = ?";
	        ps = connection.prepareStatement(sql);

	        ps.setInt(1, id);


	        ps.executeUpdate();
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}