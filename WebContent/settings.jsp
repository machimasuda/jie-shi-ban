<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editName}の編集</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">

    </head>
    <body>

   <div class ="head">
        <a href="management"class="home">ユーザー管理</a>
        <a href="logout" class="loginiNewUser" onclick ="return loginout();">ログアウト</a>
        <div class="alltitle">${editName}の編集</div></div>
        <div class="main-contents">

			<div class="hennsyuu">
			 <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                           <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post">
            <table align="center" cellspacing="9" style="text-align:left;">
                <tr><td><input name="id" value="${editUser.id}" id="id" type="hidden" />
                <label for="name">名前</label></td>
                <td style="width:220px;"><input name="name" value="${editUser.name}" id="name" placeholder="10文字以下" /><br/>
                </td></tr>

                <tr><td><label for="account">ログインID</label></td>
                <td style="width:220px;"><input type="hidden" name="beforeAccount" value="${beforeAccount}"/>
                <input name="account" value="${editUser.account}" placeholder="半角英数字の6-20文字" /><br/>
                </td></tr>

                <tr><td><label for="password">パスワード</label></td>
                <td style="width:220px;"><input name="password" type="password" id="password" placeholder="半角英数字記号の6-20文字" />
                </td>
				<tr><td><label for="checkpassword">確認用パスワード</label></td>
                <td style="width:220px;"><input name="checkpassword" type="password" id="checkpassword"/>
                </td></tr>


                <tr><td><label for="branch_id">支店名</label></td>

                <%-- ログインしている人の場合 --%>
                <td><c:if test="${loginUser.id == editUser.id}">
                <input type="hidden" name="branch_id" value="${editUser.branch_id}"/>
                <select disabled class="senntaku">
						<c:forEach items="${branches}" var="branch">
						<c:if test="${editUser.branch_id == branch.id}" >
						<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${editUser.branch_id != branch.id}" >
						<option value="${branch.id}">${branch.name}</option>
						</c:if>
						</c:forEach>
				</select>
				</c:if>

 				<%-- ログインしている人以外の場合 --%>
				<c:if test="${loginUser.id != editUser.id}">
                <select name="branch_id" class="senntaku">
						<c:forEach items="${branches}" var="branch">
						<c:if test="${editUser.branch_id == branch.id}" >
						<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${editUser.branch_id != branch.id}" >
						<option value="${branch.id}">${branch.name}</option>
						</c:if>
						</c:forEach>
				</select>
				</c:if>
				</td></tr>

               <tr><td><label for="department_id">部署・役職名</label></td>

                 <%-- ログインしている人の場合 --%>
				<td><c:if test="${loginUser.id == editUser.id}">
                <input type="hidden" name="department_id" value="${editUser.department_id}"/>
                <select disabled class="senntaku">
						<c:forEach items="${departments}" var="department">
							<c:if test="${editUser.department_id == department.id}">
							<option value="${department.id}" selected>${department.name}</option>
							</c:if>
							<c:if test="${editUser.department_id != department.id}">
							<option value="${department.id}">${department.name}</option>
							</c:if>
						</c:forEach>
				</select>
				</c:if>

				<%-- ログインしている人以外の場合 --%>
				<c:if test="${loginUser.id != editUser.id}">
                <select name="department_id" class="senntaku">
					<c:forEach items="${departments}" var="department">
							<c:if test="${editUser.department_id == department.id}">
							<option value="${department.id}" selected>${department.name}</option>
							</c:if>
							<c:if test="${editUser.department_id != department.id}">
							<option value="${department.id}">${department.name}</option>
							</c:if>
					</c:forEach>
				</select>
				</c:if></td></tr>
</table>
				<br/>
            	<input class="botann" type="submit" value="更新" /><br />
            	<input type="hidden" name="editName" value="${editName}" />
            	<input type="hidden" name="user_account" value="${user_account}" />

            </form>
            </div>
        </div>

    </body>
	<div id="footer">Copyright(c)Masuda Machi</div>
</html>