<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link href="./css/style.css" rel="stylesheet" type="text/css">
    	<title>新規投稿</title>
    </head>
    <body>

		<div class ="head">
    		<a href="./" class="home">ホーム</a>
    <a href="logout" class="loginiNewpost" onclick ="return loginout();">ログアウト</a>
    		<div class="alltitle">新規投稿</div>

		</div>
            <form action="newPost" method="post">
            	<div class="messageform">
            		<c:if test="${ not empty errorMessages }">
               		 <div class="errorMessages">
                    	<ul >
                        	<c:forEach items="${errorMessages}" var="message">
                            	<li ><c:out value="${message}" />
                        	</c:forEach>
                    	</ul>
                	</div>
               		<c:remove var="errorMessages" scope="session" />
            		</c:if>

               <label for="title">件名</label>
                 <input name="title"value="${message.title}" placeholder="30文字以下" id="title" /> <br />

                <label for="category">カテゴリー</label>
                <input name="category" value="${message.category}" placeholder="10文字以下" id="category" /> <br />

				<label for="text">本文</label>
                <div class="te"><textarea class="text"  name="text" cols="40" rows="7" placeholder="1000文字以下">${message.text}</textarea>
                </div>
              	<br />
                <input class="botann" type="submit" value="投稿" /><br /><br />

            </form>
		</div>
    </body>
    <div id="footer">Copyright(c)Masuda Machi</div>
</html>