<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板</title>
         <link href="./css/style.css" rel="stylesheet" type="text/css">
   </head>
    	<body>
			<div class ="head">
        		<c:if test="${ not empty loginUser }">
					<a href="newPost" class="newPost"><i class="fa fa-caret-right"></i>新規投稿</a>
        				<c:if test="${loginUser.department_id == 1 && loginUser.branch_id == 1 }">
        					<a href="management" class="yu_za">ユーザー管理</a>
        					<a href="logout" class="logini" onclick ="return loginout();">ログアウト</a>
        				</c:if>
        				<c:if test="${loginUser.department_id != 1 && loginUser.branch_id != 1 }">
        				<a href="logout" class="loginippann" onclick ="return loginout();">ログアウト</a>
						</c:if>

        				<div class="alltitle">掲示板</div>

				</c:if>


			</div>

        	<div class="main-contents"></div>
				<div id="usermessages">
				<div class="lll">
				<div class="onamae">ログイン中：${loginUser.name}さん</div>
					<form action ="./" class="categoryDate" method ="get">
						<br/>カテゴリー検索 <br/><input  name ="Category" value ="${Category}"/><br/>

						<div class="date">日付検索 <br/>
						<input type ="date" name ="fromDate" value ="${fromDate}"/>から<br/>
							<input type ="date" name ="toDate" value ="${toDate}"/>まで<br/><br/>
							<input class="sarch" type="submit" value="検索">
							</div><br/>
					</form>
				</div>
					<div class="errorMessages">
						<c:if test="${ not empty errorMessages }">
                   			 <ul >
                        		<c:forEach items="${errorMessages}" var="message">
                           			<li style="list-style: none;"><c:out value="${message}" />
                        		</c:forEach>
                   			 </ul>
                			<c:remove var="errorMessages" scope="session"/>
            			</c:if>
            			<c:if test="${empty messages}">該当する投稿がありません</c:if>
           			</div>

    				<c:forEach items="${messages}" var="usermessage">
           			 	<div class="message">
                			<div class="title-account">
                			<h1 class="messagetitle"><c:out value="${usermessage.title}" /></h1>
                			<div class="messagename">投稿者：<c:out value="${usermessage.name}" /></div>


                    		<div class="messagecategory">カテゴリー：<c:out value="${usermessage.category}" /></div>
                    		<div class="messagetext">
								<c:forEach var="text" items="${fn:split(usermessage.text,'
								')}" ><c:out value="${text}" /><br></c:forEach>
							<div class="messagedate" text align="right">
							<br/><font size="3">投稿日時：<fmt:formatDate value="${usermessage.createdDate}"
                			pattern="yyyy/MM/dd HH:mm:ss" /></font></div>
                    		</div>
                    		</div>
						<c:if test="${loginUser.id == usermessage.user_id}">
            				<form action ="messagedelete" method ="post">
            					<input type="hidden" name="message_id" value="${usermessage.id}">
            					<input class="botanndelete" type="submit" value="削除" onclick ="return deletedcheckM();">
            				</form>
            			</c:if>
            			</div><br />

            	<div class="usercomments">
    				<c:forEach items="${comments}" var="usercomment">
    					<c:if test= "${usermessage.id == usercomment.message_id}">
           			 		<div class="comment">
                			<div class="title-account">
                			<div class="commentname"><c:out value="${usercomment.name}" />さんからのコメント</div>
							<div class="commenttext">
                    			<c:forEach var="text" items="${fn:split(usercomment.text,'
									')}" ><c:out value="${text}" /><br></c:forEach>
                   				<br/><div class="commentdate" text align="right"><font size="3">投稿日時：<fmt:formatDate value="${usercomment.createdDate}"
                				pattern="yyyy/MM/dd HH:mm:ss" /></font></div>
                   			</div>
                    		</div>

            				</div>

						<c:if test= "${loginUser.id == usercomment.user_id}">
            				<form action ="commentdelete" method ="post">
            					<input type="hidden" name="comment_id" value="${usercomment.id}">
            					<input class="botanndelete" type="submit" value="削除" onclick ="return deletedcheckC();">
            				</form><br/>
						</c:if>
            			</c:if>
    				</c:forEach>
				</div>

						<div class="form-area">
        					<form action="commentServlet" method="post">コメント<br />
            					<input type="hidden" name="message_id" value="${usermessage.id}" >


            					<c:if test= "${returnComment.message_id != usermessage.id}">
            						<textarea class="tweet-box" name="comment"cols="35" rows="5" placeholder="500文字以下"></textarea>
            					</c:if>

            					<c:if test= "${returnComment.message_id == usermessage.id}">
            						<textarea class="tweet-box" name="comment"cols="35" rows="5" placeholder="500文字以下">${returnComment.text}</textarea>
            							<c:if test="${ empty errorMessages }">
                							<c:remove var="returnComment" scope="session"/>
            							</c:if>
            					</c:if>


            					<br />
            					<input class="botann" type="submit" value="コメント"><br />
        					</form>
						</div>
						<br /><br />
    				</c:forEach>


    			<script>
						function deletedcheckM() {
    						return confirm('このメッセージを削除しますか？');
						}
						function deletedcheckC() {
    						return confirm('このコメントを削除しますか？');
						}
						function loginout(){
							return confirm('ログアウトしますか？');
						}
						</script>
			</div>
			</body>
			<div class=copyrightTop> Copyright(c)Masuda Machi</div>

</html>