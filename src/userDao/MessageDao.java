package userDao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append(" title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // title
            sql.append(", ?");//text
            sql.append(", ?");//category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?");//user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getTitle());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getUser_Id());

            System.out.println(ps.toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public List<Message> getUserMessage(Connection connection, int limitNum) {
		//
		return null;
	}

	public void delete(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "delete from messages where id = ?";
	        ps = connection.prepareStatement(sql);

	        ps.setInt(1, id);
	        ps.executeUpdate();
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}

