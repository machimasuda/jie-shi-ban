package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;


@WebServlet("/IsdeletedServlet")
public class IsdeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	 protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {


	    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");


		int user_id = Integer.parseInt(request.getParameter("user_id"));
		int is_deleted = Integer.parseInt(request.getParameter("is_deleted"));

		if( user_id!=user.getId()){
        new UserService().isdeleted(user_id, is_deleted);

		}
		response.sendRedirect("management");
	}
}
