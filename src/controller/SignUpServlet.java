package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	HttpSession session = request.getSession();


    	List<Branch> branchList = new BranchService().getBranch();
    	session.setAttribute("branches", branchList);

    	List<Department> departmentList = new DepartmentService().getDepartment();
    	session.setAttribute("departments", departmentList);

    	User User = (User) session.getAttribute("ExistUser");
    	String user_account = request.getParameter("account");

    	User existUser = new UserService().getUser(user_account);
        request.setAttribute("existUser", existUser);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(request.getParameter("branch_id"));
            user.setDepartment_id(request.getParameter("department_id"));

            new UserService().register(user);

            response.sendRedirect("management");
        } else {
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setAccount(request.getParameter("account"));
            user.setBranch_id(request.getParameter("branch_id"));
            user.setDepartment_id(request.getParameter("department_id"));

            session.setAttribute("errorMessages", messages);
            request.setAttribute("User", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);


        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
    	String account = request.getParameter("account");
    	String beforesignAccount = request.getParameter("beforesignAccount");
        String password = request.getParameter("password");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");
        String checkpassword = request.getParameter("checkpassword");



        User existUser = new UserService().getUser(account);
        request.setAttribute("existUser", existUser);

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }else if (name.length() >= 10)  {
            messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isBlank(account) == true) {
            messages.add("ログインIDを入力してください");
        }else if  (account.length() > 20 || account.length() < 6)   {
            messages.add("ログインIDは6文字以上20文字以下で入力してください");
        }
        if (!account.matches("^[0-9a-zA-Z]*$"))  {
            messages.add("ログインIDは半角英数字で入力してください");
        }else if(!beforesignAccount.equals(account) && existUser != null){
        	messages.add("このログインIDは既に使われています");
        }

        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }else {
        	if(password.length() > 20 || password.length() < 6){
        		messages.add("パスワードは6文字以上20文字以下で入力してください");
        	} else if (!password.matches("^[a-zA-Z0-9 \\-\\/\\:\\-\\@\\[\\-\\{\\-\\~]*$")) {
                messages.add("パスワードは半角英数字記号で入力してください");
            }else if(StringUtils.isBlank(checkpassword) == true){
            	messages.add("確認用パスワードを入力してください");
            }else{
            	if(!password.equals(checkpassword)){
                	messages.add("パスワードが一致しません");
                }
            }
        }


        if (StringUtils.isBlank(branch_id) == true) {
            messages.add("支店名を選択してください");
        }
        if (StringUtils.isBlank(department_id) == true) {
            messages.add("部署・役職名を選択してください");
        }

        if(branch_id.equals("1")){
        	if(!(department_id.equals("1") ||department_id.equals("2"))){
        	messages.add("支店名と部署・役職名の組み合わせが不正です");
        	}
        }
        if(!branch_id.equals("1")){
        	if(department_id.equals("1") ||department_id.equals("2")){
        	messages.add("支店名と部署・役職名の組み合わせが不正です");
        	}
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
