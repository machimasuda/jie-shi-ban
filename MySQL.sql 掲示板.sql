show databases
use masuda_machi;
create table user(
	id          INTEGER       AUTO_INCREMENT PRIMARY KEY,
    account     VARCHAR(20)   UNIQUE NOT NULL,
    password    VARCHAR(20)   NOT NULL,
	name        VARCHAR(20),
    branch_id       VARCHAR(20),
    department_id	VARCHAR(20),
    created_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP 
);

create table branches(
	id          INTEGER       AUTO_INCREMENT PRIMARY KEY,
	branch_name VARCHAR(20)
);

create table departments(
	id          INTEGER       AUTO_INCREMENT PRIMARY KEY,
	userdepartment_name VARCHAR(20)
);

